var fs = require ('fs');
var path = require ('path');

module.exports = function getFilteredList(nameDir, extToPrint, callback)
{
	var res = [];
	fs.readdir(nameDir, function call2(err, list)
	{
		if (err)
		{
			return callback(err);
		}
		else
		{
		var j = 0;
		for (var i = 0; i <= list.length; i++)
		{	
			if (path.extname(list[i]) == '.' + extToPrint)
			{
				res[j] = list[i];
				j++;
			}
		}
		return callback(null, res);
		}
	}) ;	
}
